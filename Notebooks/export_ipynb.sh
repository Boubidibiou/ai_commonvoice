#!/bin/sh

jupyter nbconvert --to python deeplearning.ipynb --output ../scripts/deeplearning.py
jupyter nbconvert --to pdf deeplearning.ipynb --output ../deeplearning.pdf
