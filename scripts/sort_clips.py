#!/usr/bin/env python
# coding: utf-8

import os
import csv

# global variables
print "Entrez une langue : (en/fr)"
language = raw_input()

tsv_filename = "all"
tsv_path = "../data/"+ language +"/tsv/"
clips_path = "../data/"+ language +"/clips/"
data_path = '../data/' + language +"/"

CLIP_MIN_DURATION = int(input("Min clip duration in sec : "))
CLIENT_MIN_CLIPS = int(input("Min nb of file per speaker : "))
CLIENT_MAX_CLIPS = int(input("Max nb of file per speaker : "))

CLIP_MIN_DURATION = 16000*16*CLIP_MIN_DURATION/8 # SMAPLE_FREQ x BIT DEPTH x LEN / bitToByte

# 1 - Only keeps clips with desired duration #####################
total_files = 0
valid_file = 0
# get the total nb of file :
with open(tsv_path + tsv_filename + '.tsv', 'r') as tsvinput:
        tsvreader = csv.reader(tsvinput, delimiter="\t")
        r_row = next(tsvreader)
        for row in tsvreader:
            total_files += 1

print "Processing %d data" % total_files

with open(tsv_path + tsv_filename + ".tsv", 'r') as tsvinput:
    with open(tsv_path + "tmp.tsv", 'w') as tsvoutput:
        tsvreader = csv.reader(tsvinput, delimiter="\t")
        tsvwriter = csv.writer(tsvoutput, delimiter="\t", lineterminator='\n')

        # skip the non data first line
        row = next(tsvreader)
        tsvwriter.writerow(row)
        i = 0
        for row in tsvreader:
            i+=1
            if i%1000 == 0:
                print "\r(%d/%d)" % (i, total_files),

            # using file size to estimate sound duration
            try:
                l = os.path.getsize(clips_path + row[1] + ".wav")
            except:
                l = 0
                print "No such file '%s'"%(clips_path + row[1] + ".wav")
            if(l >= CLIP_MIN_DURATION): # 8 bits mp3
                valid_file += 1
                # if duration ok, we write it in our export tsv
                tsvwriter.writerow(row)
        print ''
print "There is %d valid files"%valid_file

print "Press <ENTER> to continue..."
raw_input()

# 2 - Sort the speakers ############################
valid_speakers = [] # usefull to discard non valid speaker in future sort
print "Sorting speakers with required nb of clip"
with open(tsv_path + "tmp.tsv", 'r') as tsvinput:
        tsvreader = csv.reader(tsvinput, delimiter="\t")

        row = next(tsvreader)

        last_speaker = '';
        current_speaker_clip_nb = 1 # we gonna count the nb of clip per speaker

        i = 0
        for row in tsvreader:
            i+=1
            if i%100 == 0:
                print "\r(%d/%d)" % (i, valid_file),
            if last_speaker == row[0]:
                # same speaker: just count his clips
                current_speaker_clip_nb += 1
            else:
                # new speaker
                if current_speaker_clip_nb > CLIENT_MIN_CLIPS:
                    valid_speakers.append(last_speaker)
                current_speaker_clip_nb = 1 # reset
            last_speaker = row[0]
        print ''

print "There is %d valid speakers" % len(valid_speakers)

# 3 - Make the wav.scp ####################################
nb_clips = 0 # count the nb of clip we have
with open(tsv_path + "tmp.tsv", 'r') as tsvinput:
    with open(data_path + "wav.scp", 'w') as wavscp:
            tsvreader = csv.reader(tsvinput, delimiter="\t")
            row = next(tsvreader)

            last_speaker = ''
            current_speaker_clip_nb = 0
            for row in tsvreader:
                c = row[0]
                if c in valid_speakers:
                    if c != last_speaker:
                        # new speaker
                        current_speaker_clip_nb = 0
                        last_speaker = c

                    if current_speaker_clip_nb < CLIENT_MAX_CLIPS:
                        sid = valid_speakers.index(c)
                        wavscp.write("speaker%d#clip%d clips/%s.wav\n"%(sid, current_speaker_clip_nb, row[1]))
                        current_speaker_clip_nb += 1
                        nb_clips += 1

print "There is %d clips" % nb_clips
print "wav.scp created"
print "END"
