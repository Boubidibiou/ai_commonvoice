#!/usr/bin/env python
# coding: utf-8

import numpy as np
import kaldi_io
import csv
from os import remove

NOISE_DIR_NB = 0
CLIP_LEN = 300 # 4s
OVERLAP_LEN = 100 # 1s

CLEANUP = False # remove the mfcc and vad files to keep only post processed mfcc

language = "en"

data_path = "../data/"+ language + "/"
noised_data_path = "noised%d/"
# export_path = "../data/"+ language + "/mini_clips/"

# feats = {k:m for k,m in kaldi_io.read_mat_ark(data_path + 'featsnorm.ark')}
# print "feats impote"

vad = {k:m for k,m in kaldi_io.read_vec_flt_ark(data_path + 'vad.ark')}
print "vad impote"

noised_feats = []
for i in range(NOISE_DIR_NB):
    noised_feats.append({k:m for k,m in kaldi_io.read_mat_ark(data_path + noised_data_path%i + 'featsnorm.ark')})

print "noised feats impote"

export_feats = {}

def cut(c, f, v, noises):
    mfcc = f
    # Apply the vad to the mfcc
    for line in reversed([i for i, x in enumerate(v) if x == 0]):
        # get all the lines to delete, from last to first
        mfcc = np.delete(mfcc, line, 0) # delete line from mfcc
        for j in range(len(noises)):
            noises[j] = np.delete(noises[j], line, 0)

    nb_overlap = 0
    s =  np.shape(mfcc)[0]
    while 1:
        s -= CLIP_LEN
        if(s < 0):
            break # not enought values to make another mini clip
        nb_overlap += 1
        s += OVERLAP_LEN

    if nb_overlap==0:
        print "/!\\ CLEANED UP CLIP IS TOO SHORT (%d < %d) /!\\" % (s, CLIP_LEN)
        return 0

    # Export all the mini clips
    for i in range(nb_overlap):
        new_name = c + "_mini%d"%i
        export_feats[new_name] = mfcc[i*(CLIP_LEN - OVERLAP_LEN):i*(CLIP_LEN - OVERLAP_LEN)+CLIP_LEN]
        for j in range(len(noises)):
            new_name = c + "n%d_mini%d"%(j, i)
            export_feats[new_name] = noises[j][i*(CLIP_LEN - OVERLAP_LEN):i*(CLIP_LEN - OVERLAP_LEN)+CLIP_LEN]

    return nb_overlap

s = 0
i = 0
# for clip, feat in feats.iteritems():
for clip, feat in kaldi_io.read_mat_ark(data_path + 'featsnorm.ark'):
    i += 1
    if i%1000 == 0:
        print i
    s += cut(clip, feat, vad.pop(clip), [n.pop(clip) for n in noised_feats])
print "calcul feats effectue : %d mini feats"%s


with kaldi_io.open_or_fd(data_path + 'minifeats.ark','wb') as f:
    for k,v in export_feats.iteritems(): kaldi_io.write_mat(f, v, k)
print "export effectue"
