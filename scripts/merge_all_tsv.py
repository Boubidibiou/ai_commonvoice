#!/usr/bin/env python
# coding: utf-8

import csv

tsv_names = ['train.tsv', 'test.tsv', 'dev.tsv', 'validated.tsv']
output_tsv = "all.tsv"
export_tsv_path = "../data/en/tsv/"
tsv_path = "../data/en/tsv/original_tsv/"

# get all the data into a list
# data = [
#     ['clientX', ['clientX_clip1', 'clientX_clip2', 'clientX_clip3']],
#     ['clientY', ['clientY_clip1', 'clientY_clip2']]
# ]

data = []
last_client = ''
curr_client_id = 0
nb_client = -1
clients = []
nb_file = 0

for tsv in tsv_names:
    with open(tsv_path + tsv, 'r') as tsvinput:
        tsvreader = csv.reader(tsvinput, delimiter='\t')
        next(tsvreader)

        for row in tsvreader:
            nb_file += 1
            client = row[0]
            path = row[1]
            if client != last_client:
                last_client = client
                if client in clients:
                    i = clients.index(client)
                    data[i][1].append(path)
                    curr_client_id = i
                else:
                    clients.append(client)
                    data.append([client, [path]])
                    nb_client += 1
                    curr_client_id = nb_client
            else: # still with the same client :
                data[curr_client_id][1].append(path)

print "nb of clients : %d"%nb_client
print "nb of files: %d"%nb_file
print "average nb clips per client : %d"%(sum([len(d[1]) for d in data])/len(data))


i = 0
with open(export_tsv_path + output_tsv, 'w') as tsvoutput:
    tsvwriter = csv.writer(tsvoutput, delimiter="\t", lineterminator='\n')
    tsvwriter.writerow(['client id', 'file path'])
    for c in range(len(data)):
        for f in data[c][1]:
            tsvwriter.writerow(["%d"%c, f])
            i+=1
print "Wrote %d lines "%i
