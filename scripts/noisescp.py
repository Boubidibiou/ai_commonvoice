#!/usr/bin/env python
# coding: utf-8

import csv

file_tsv = "noises.tsv"
path_to_tsv = "../data/fr/tsv/"


data = []

# structure of the array 'data' :
# data = [ [client_name0, [filename1, filename2, filename3]],
# [client_name2, [filename1, filename2, filename3, filename4, filename5]],
# [client_name3, [filename1, filename2]] ]

# Get the data from the tsv and store it in the array 'data'
with open(path_to_tsv + file_tsv, 'r') as tsvinput:
    tsvreader = csv.reader(tsvinput, delimiter='\t')
    next(tsvreader)

    for row in tsvreader:
        path = "../data/musan/%s/%s/"%(row[0], row[1])
        name = row[2]
        data.append((name, path + name))

# Make the scp files from the data
with open(path_to_tsv + "noise.scp", 'wa') as wavscp:
    for X in data:
        wavscp.write("%s %s\n"%X)

print "Done"
