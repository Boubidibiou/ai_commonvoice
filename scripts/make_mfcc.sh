#!/bin/sh

cd ../data/en/

# Calcul de la mfcc et application de la cmvn
compute-mfcc-feats --sample-frequency=16000 --frame-length=25 --low-freq=20 --high-freq=-400 --num-ceps=20 --snip-edges=false scp:wav.scp ark:feats.ark
compute-cmvn-stats ark:feats.ark ark:cmvn.ark
apply-cmvn --norm-means=true --norm-vars=true ark:cmvn.ark ark:feats.ark ark:featsnorm.ark
# apply-cmvn --norm-means=true --norm-vars=true ark:- ark:feats.ark ark:featsnorm.ark | compute-cmvn-stats ark:feats.ark ark:-

# Calcul de la vad
compute-vad --vad-energy-threshold=5.5 --vad-energy-mean-scale=0.5 --vad-proportion-threshold=0.12 --vad-frames-context=2 ark:feats.ark ark:vad.ark
rm feats.ark cmvn.ark

# cd noised0/
# compute-mfcc-feats --sample-frequency=16000 --frame-length=25 --low-freq=20 --high-freq=-400 --num-ceps=20 --snip-edges=false scp:wav.scp ark:feats.ark
# compute-cmvn-stats ark:feats.ark ark:cmvn.ark
# apply-cmvn --norm-means=true --norm-vars=true ark:cmvn.ark ark:feats.ark ark:featsnorm.ark
# rm feats.ark cmvn.ark
#
# cd ../noised1/
# compute-mfcc-feats --sample-frequency=16000 --frame-length=25 --low-freq=20 --high-freq=-400 --num-ceps=20 --snip-edges=false scp:wav.scp ark:feats.ark
# compute-cmvn-stats ark:feats.ark ark:cmvn.ark
# apply-cmvn --norm-means=true --norm-vars=true ark:cmvn.ark ark:feats.ark ark:featsnorm.ark
# rm feats.ark cmvn.ark
#
# cd ../noised2/
# compute-mfcc-feats --sample-frequency=16000 --frame-length=25 --low-freq=20 --high-freq=-400 --num-ceps=20 --snip-edges=false scp:wav.scp ark:feats.ark
# compute-cmvn-stats ark:feats.ark ark:cmvn.ark
# apply-cmvn --norm-means=true --norm-vars=true ark:cmvn.ark ark:feats.ark ark:featsnorm.ark
# rm feats.ark cmvn.ark
