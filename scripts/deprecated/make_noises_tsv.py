import csv

tsv_path = "../data/fr/tsv/"

noise_tree = [
	['music', [
		 ['hd', [i for i in range(75)]],
		 ['fma', [i for i in range(128)]],
		 ['fma-wa', [i for i in range(93)]],
		 ['jamendo', [i for i in range(217)]],
		 ['rfm', [i for i in range(147)]]
		 ]
	]
]

# def get_name_from_tree(i, j, k):
# 	return "%s-%s-%04d.wav"%(noise_tree[i][0], noise_tree[i][1][j][0], k)

# def get_path_from_tree(i, j):
# 	return "%s/%s/"%(noise_tree[i][0], noise_tree[i][1][j][0])

i=0
with open(tsv_path + "noises.tsv", 'w') as tsvout:
	tsvwrit = csv.writer(tsvout, delimiter="\t", lineterminator='\n')
	for f in noise_tree:
		for sf in f[1]:
			for k in sf[1]:
				tsvwrit.writerow([f[0], sf[0], "%s-%s-%04d"%(f[0], sf[0], k)])
				i+=1
print "There is %d noises"%i
print "Exported in 'noises.tsv'"
