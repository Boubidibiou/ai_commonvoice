#!/usr/bin/env python
# coding: utf-8

import csv
from os import remove
from shutil import copyfile

print "Entrez une langue : (en/fr)"
language = raw_input()
tsv_filename = "all_sorted_noised_mini.tsv"

RATIO = 0.2 # Ration for test/train cut. 0.2 means 20% of data go to test
CLIENT_MIN_CLIPS = 200
# TODO : make a better cut of data to test

path_to_tsv = "../data/"+ language +"/tsv/"
path_to_data = "../data/"+ language +"/mini_clips/"
path_to_train = "../data/"+ language +"/train/"
path_to_test = "../data/"+ language +"/test/"

# Cut in batch, only in tsv files
with open(path_to_tsv + tsv_filename, 'r') as tsvinput:
    with open(path_to_tsv + "train.tsv", 'w') as tsvoutput_train:
        with open(path_to_tsv + "test.tsv", 'w') as tsvoutput_test:
            tsvreader = csv.reader(tsvinput, delimiter="\t")
            tsvtest = csv.writer(tsvoutput_test, delimiter="\t", lineterminator='\n')
            tsvtrain = csv.writer(tsvoutput_train, delimiter="\t", lineterminator='\n')
            row = next(tsvreader)
            tsvtest.writerow(row)
            tsvtrain.writerow(row)

            last_client = ''
            tr = 0
            te = 0
            c = 0
            for row in tsvreader:
                c += 1
                client = row[0]
                if client != last_client:
                    last_client = client
                    c = 0

                if c <= RATIO*CLIENT_MIN_CLIPS:
                    # this goes for test
                    tsvtest.writerow(row)
                    te += 1
                else:
                    tsvtrain.writerow(row)
                    tr += 1

print "Total data : %d test, %d train"%(te, tr)

# print "Press <ENTER> to move the mfcc files in the respective folders..."
# raw_input()
#
# # copy mfcc to respective folders
# def move_mfcc_to_batch(batch_tsv, batch_path, total):
#     print "Copying from %s"%batch_tsv
#     with open(path_to_tsv + batch_tsv, 'r') as tsvinput:
#         tsvreader = csv.reader(tsvinput, delimiter="\t")
#         row = next(tsvreader)
#
#         i = 0
#         for row in tsvreader:
#             i+=1
#             print "\r(%d/%d)" % (i, total),
#             copyfile(path_to_data + row[1] + ".mfcc", batch_path + row[1] + ".mfcc")
#         print ''
# move_mfcc_to_batch("batch_test.tsv", path_to_test, te)
# move_mfcc_to_batch("batch_train.tsv", path_to_train, tr)
