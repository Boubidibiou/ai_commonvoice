# noises : [8, 15, 16, 33, 35, 37, 39, 44, 45, 46, 49, 50, 55, 56, 60, 61, 62, 63, 68, 76, 77, 79, 80, 82, 84]
# nb of valid noise 25

# noises : [45, 49, 50, 61, 76, 77, 84] - teste OK
# nb of valid noise 7

import wave
import numpy as np
import csv
from random import randint

NB_MIX = 3

language = "fr"
clips_path = "../data/"+ language +"/clips_sorted/"
export_path = "../data/"+ language +"/clips_sorted/noised/"
tsv_path = "../data/"+ language +"/tsv/"
base_noise_path = "../data/musan/"

noises = []
noises_path = []
nb_noises = 0
with open(tsv_path + "noises.tsv", 'r') as tsvinput:
    tsvreader = csv.reader(tsvinput, delimiter="\t")
    for row in tsvreader:
        noises.append(row[2]) # take the third column : filename
        noises_path.append(row[0]+"/"+row[1]+"/") # # take the first and second column : the two folders
        nb_noises += 1


print "Mixing each clip with %d noises selected from %d..."%(NB_MIX, nb_noises)
print "Exporting in 'all_sorted_noised.tsv'"

with open(tsv_path + "all_sorted.tsv", 'r') as tsvinput:
    with open(tsv_path + "all_sorted_noised.tsv", 'w') as tsvout:
        tsvreader = csv.reader(tsvinput, delimiter="\t")
        tsvwriter = csv.writer(tsvout, delimiter="\t", lineterminator='\n')

        row = next(tsvreader) # pass the first row with the label
        tsvwriter.writerow(row)

        i = 0
        for row in tsvreader:
            # loading bar
            print "\r(%d/%d)" % (i, 5000),
            i+=1

            wrow = [row[0], 0] # create a new row we will write in the new tsv

            # add non noised to list
            tsvwriter.writerow([row[0], row[2]])

            for k in range(NB_MIX):
                # select a random noise
                seed = randint(0, nb_noises-1)

                new_name = row[2] + "_" +noises[seed] # make the new name of the file "speakerK#clipX_noiseY.wav"
                wrow[1] = new_name # add this new name in the row
                tsvwriter.writerow(wrow) # write the row

                # load the clip and the noise
                fnames =[clips_path+row[2]+'.wav', base_noise_path+noises_path[seed]+noises[seed]]
                wavs = [wave.open(fn) for fn in fnames]
                frames = [w.readframes(w.getnframes()) for w in wavs]

                # here's efficient numpy conversion of the raw byte buffers
                # '<i2' is a little-endian two-byte integer.
                samples = [np.frombuffer(f, dtype='<i2') for f in frames]
                samples = [samp.astype(np.float64) for samp in samples]
                # mix as much as possible
                n = min(map(len, samples))
                mix = samples[0][:n] + samples[1][:n]
                # Save the result
                mix_wav = wave.open(export_path+new_name+'.wav', 'w')
                mix_wav.setparams(wavs[0].getparams())
                # before saving, we want to convert back to '<i2' bytes:
                mix_wav.writeframes(mix.astype('<i2').tobytes())
                mix_wav.close()
