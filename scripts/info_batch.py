#!/usr/bin/env python
# coding: utf-8

# show information about the batch in the tsv file
# Plot the nb of clips per clients from specified tsv file

import csv
import matplotlib.pyplot as plt
from math import sqrt

print "Enter batch file : (Exemple : batch_train.tsv)"
file_tsv = raw_input()
print "Entrez une langue : (en/fr)"
language = raw_input()
path_to_tsv = "../data/"+ language +"/tsv/"


data = []
last_client = ''
curr_client_id = 0
nb_client = -1
clients = []
nb_file = 0

with open(path_to_tsv + file_tsv, 'r') as tsvinput:
    tsvreader = csv.reader(tsvinput, delimiter='\t')
    next(tsvreader)

    for row in tsvreader:
        nb_file += 1
        client = row[0]
        path = row[1]
        if client != last_client:
            last_client = client
            if client in clients:
                i = clients.index(client)
                data[i][1].append(path)
                curr_client_id = i
            else:
                clients.append(client)
                data.append([client, [path]])
                nb_client += 1
                curr_client_id = nb_client
        else: # still with the same client :
            data[curr_client_id][1].append(path)

X = [i for i in range(len(data))]
Y = [len(d[1]) for d in data]

print "nb of clients : ", nb_client
print "nb of files: ", nb_file
print "Files per client : ", Y
moy =  sum([len(d[1]) for d in data])/len(data)
var = sqrt(sum([len(d[1]) ** 2 for d in data])/len(data) - moy**2)
print "average nb clips per client : ", moy
print "variance : ", int(var)


plt.figure(dpi=500)
plt.plot(X, Y)
plt.title("NB of clips per client")
plt.xlabel('Client id')
plt.ylabel('Nb of clips')
plt.axis([0, nb_client, 0, max(Y)])
plt.show()
