#!/usr/bin/env python
# coding: utf-8

import csv

file_tsv = "all_sorted.tsv"
tsv_path = "../data/en/tsv/"
# data_path = "../data/en/sorted_clips/"
data_path = ''

data = []
last_client = ''
curr_client_id = 0
nb_client = -1
clients = []

# structure of the array 'data' :
# data = [ [client_name0, [filename1, filename2, filename3]],
# [client_name2, [filename1, filename2, filename3, filename4, filename5]],
# [client_name3, [filename1, filename2]] ]

# Get the data from the tsv and store it in the array 'data'
with open(tsv_path + file_tsv, 'r') as tsvinput:
    tsvreader = csv.reader(tsvinput, delimiter='\t')
    next(tsvreader)

    for row in tsvreader:
        client = row[0]
        path = row[2]
        if client != last_client:
            last_client = client
            if client in clients:
                i = clients.index(client)
                data[i][1].append(path)
                curr_client_id = i
            else:
                clients.append(client)
                data.append([client, [path]])
                nb_client += 1
                curr_client_id = nb_client
        else: # still with the same client :
            data[curr_client_id][1].append(path)

# Make the scp files from the data
with open(tsv_path + "wav.scp", 'wa') as wavscp:
    with open(tsv_path + "spk2utt.scp", "w") as spk2utt:
        with open(tsv_path + "utt2spk.scp", "w") as utt2spk:
            for X in data:
                client = X[0]
                spk2utt.write(client + " ")
                for clip in X[1]:
                    spk2utt.write(clip + " ")
                    utt2spk.write("%s %s\n"%(clip, client))
                    wavscp.write("%s %s%s.wav\n"%(clip, data_path, clip))
                spk2utt.write("\n")

print "Done"
