# Premier Projet
## Detection de locuteur en milieu ferme

### Dataset

Les clips audio utilisés sont issus du projet 'CommonVoice' de Mozilla https://voice.mozilla.org/en/dataset
Le dataset francais est utilisé ici.

Le script *sort_clip* permet de garder que les clips durant plus de Xs et ayant un locuteur avec plus de Y clips.

### MFCC et CMVN

Le script *cmp_kaldi_python* permet de voir les différences entre les mfcc calculees par kaldi et python. Les différences résident dans la taille de la matrice, mais aussi dans les valeurs.

Un plot de la mfcc de kaldi :
<div class="center">
  <img src="images/kaldi_mfcc.png">
</div>

Et un venant de python :
<div class="center">
  <img src="images/python_mfcc.png">
</div>

Nous allons donc preferer kaldi à python pour le calcul des mfcc, kaldi etant plus performant et ayant plus de possibilitées.
Cependant, meme avec la mfcc *pure* (cad sans vad ou autre calcul) calculee par kaldi, le DNN n'arrive pas a des resultats concluants.

### VAD et overlap

Deux scripts de test (*play_data* et *plot_data*) permet de visualiser et écouter les donnees.  
Ceci a mis en avant le fait que parmis les clips de Xs, **certain clips étaient constitue de plus de silence que de parole**.  

Stft mettant en valeur les silences dans les clips :
<div class="center">
  <img src="images/stft.png">
</div>


Une solution à donc été choisie : utiliser la VAD (i.e. Voice Activation Detection)
Cela est fait dans le script *make_mini_clip* qui va appliquer la VAD, découper dans des clips de 4s ou plus des 'mini clips' de 3s, en overlapant de 1s maximum chaque clip.

### Overfitting

Une des raison soupsonnee de la difference entre la precision en train (90%+) et en test est l'overfiting. Pour reduire cet effet, nous avons tente de mettre en place des couches dropout, et aussi des regulation L1 (pour eviter des matrices de poid avec des valeurs trop eleve).
Cependant, la regulation L1 semble perturber l'apprentissage des que sa valeur exede 0.005.
Une solution pas encore mis en place est le earlystop : mais elle necessite un batch dev (je crois).

### Batchs

Plusieurs batchs ont été réalisé.
Le script *info_batch.py* permet de donner des informations relative a un batch (listé dans un tsv).
Ce script montre le nombre de clips par client, la moyenne et la varaiance de clip par client et autre...  
Un premier ete compose de 120 clients pour quelques 10000 clips, avec 10 clips minimum par client.
Ce premier batch n'ayant pas ete comcluant, nous nous sommes tournes vers un batch de 39 clients pour environ 8000 clips, helas peu concluant (acc test ~30%).
Apres avoir remarque que moins de 30% des clients ete proprietaire de plus de 70% des clips, nous avons fait plusieurs batch :  

    batch11 : (un batch simple)  
    11 clients  
    5687 clips  
    min 200 clips per client  
    no max  

    batch42 :  (un batch modéré)  
    42 clients  
    4727 clips  
    min 37 clips per client  
    max 287 clips per client  
    average nb clips per client :  109  
    variance :  63  
    
    batch112 : (un batch plus complexe)
    112 clients
    7957 cilps  
    min 12 clips per client  
    max 366 clips per client  
    average nb clips per client :  71   
    variance : 70  

L'evolution de la precision en test au cours des epoch :
<div class="center">
  <img src="images/model_3_allbatch_val_acc.png">
</div>
bleu : batch11  
rouge : batch42  
rose : batch112 

### Bruit

Du bruit a ete rajoute sur les clips. Les bruits viennent du dataset musan et nous les avons trie pour ne conserver que les bruits 'long' (+ de 20 sec) pour etre sur davoir un recouvrement complet du clips.
X bruits sont utilises, pour chaque clip, 3 bruit selectione aleatoirement sont appliques.
Puis 1/3 des clips generes sont abandones.
Dans notre cas, sela nous donne un batch final de 5000 + 5000x2/3 = 15 000 clips

Probleme : lors du mix, le bruit est plus fort que la voix

### Big data

Avec la nouvelle taille de nos dataset (50 000 minimum), plusieurs problemes se posent.

#### La taille prise par les donnees sur le disque

plain ascii : 1 data = 95,2 kB (toto.mfcc)
jpg : 1 data = 3,6 kB (toto_mfcc.jpg)
binaire (ark) = 3,5 kB (toto_mfcc.ark) <----

#### L'importation des donnees dans la memoire vive

Numpy importe toutes les donnes dans la ram.
Les donnees sont des tableaux de 300x20 de float64
Ce qui donne 47 952 B par donnee
Et donc 2,35Go pour les 50 000 donnees.

En changant le type des valeurs de float64 a int32:
(on multiplie par 10^8 puis on convertit en int)
1 data = 24 kB
50 000 data = 1,2 Go

int16 : PERTE DE DONNEE (5/9 chiffres grades)
1 data  = 12 kB
50 000 data = 0,6 Go

Dans tous les cas, ca fait crash mon pc

#### Progressive data loading



### DNN

#### Premier modele :  
<div class="center">
  <img src="images/DNN_1.png">
</div>

**Performances** :  
TR/TE : 80%/55% avec batch42  

**Commentaires** :  
Le reseau va tenter de trouver du sens dans les 6000 valeurs qui lui sont proposees. Cette methode est donc peu efficasse avec des batch de notre taille.

#### Deuxieme modele :  
<div class="center">
  <img src="images/DNN_2.png">
</div>

**Performances** :  
TR/TE : 93%/80% avec batch42  

**Commentaires** :  
La convulution permet d'apprendre sur un petit echantillon de la donnee complete, et est donc ideale pour notre taille de batch.
Le max pooling permet d'extraire la valeur contenant le plus d'information, et reduit la complexite des calculs.

#### Troisieme modele :  
<div class="center">
  <img src="images/DNN_3.png">
</div>

**Performances** :  
TR/TE : 95%/83% avec batch42  

**Commentaires** :  
L'avg pooling n'ajoute que peu d'information sur ce batch.

#### Comparaison :
L'evolution de la precision en test au cours des epoch :
<div class="center">
  <img src="images/batch42_model_123_val_acc.png">
</div>
orange : modele 1  
bleu : modele 2  
rouge : modele 3  

### Utilisation des scripts

Le diagrame suivant montre l'ordre d'utilisation et l'utilitée de chaque scripts.  
Rq : plus besoin de *convert_to_wav.sh* : la convertion se fait dans le sript python *sort_clips.py*. Ceci est un peu plus long mais permet deviter une copie inutile (et soulage donc un peu mon disque dur...).

<div class="center">
  <img src="images/utilisation_scripts.png">
</div>

### Dossiers

Pour que les scripts fonctionnent correctement, une arborescence a besoin d'etre respectee :  

.  
+-- Readme.md  
+-- deeplearning.pdf # permet de voir le resultat du DNN sans l'executer  
+-- scripts/  
|   +-- merge_all_tsv.py # regroupe tous les tsc dans all.tsv  
|   +-- sort_clips.py # genere client.tsv met tout les clips valide dans clips_sorted/  
|   +-- convert_to_wav.sh # plus utilise (conversion dans sort_clips.py)
|   +-- generate_mfcc.sh # genere la mfcc et la vad de clips_sorted/\*.wav  
|   +-- make_mini_clip.py # utilisation de la vad et cut a 3s avec overlap  
|   +-- cut_in_batch.py # separation dans batch_test/ et batch_train/  
|   +-- deeplearning.py # le DNN, preferer l'utilisation du Notebook ou la lecture du pdf
+-- data/  
|   +-- fr/  
|   |   +-- clips/  
|   |   |   +-- \*.mp3 # les mp3 telecharges  
|   |   +-- clips_sorted/  
|   |   |   +-- \*.wav # les wav convertis  
|   |   |   +-- \*.mfcc # les mfcc generes  
|   |   |   +-- \*.vad # les vad generes  
|   |   +-- batch_train/  
|   |   |   +-- \*.mfcc # les mini mfcc generes  
|   |   +-- batch_test/  
|   |   |   +-- \*.mfcc # les mini mfcc generes  
|   |   +-- tsv/  
|   |   |   +-- all.tsv  
+-- tf_logs/  
+-- Notebook/  
|   +-- *.ipynb # Ipython notebokk utilise pour le dev  
